import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import { AuthorizedHeader, AuthorizedFooter, Navbar } from '../Partials/index';

import { RouterModule } from '@angular/router';

import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';

import { LoaderComponent, AlertComponent } from '../Directives/index';
import { AlertService } from '../Services/index'; 

/**
 * Re-exporting modules
 *
 * Declare there modules/components/directives/pipes
 * that can be shared in many other modules
 *
 * Don't provide app-wide singleton services here
 */
@NgModule({
  imports: [
    CommonModule,
    RouterModule,

    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [
    AuthorizedHeader,
    AuthorizedFooter,
    Navbar,

    AlertComponent
  ],
  exports: [
    CommonModule,
    RouterModule,
    ReactiveFormsModule,
    AlertComponent
  ],
  providers: [
    AlertService
  ]
})
export class SharedModule {
}
