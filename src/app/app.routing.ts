import { NgModule } from '@angular/core';

import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './Guard/index';
import { MainLayoutComponent, AuthLayoutComponent } from './Layouts/index';

import { HomeComponent } from './Modules/Home/index';
import { SubjectsComponent } from './Modules/Subjects/index';
import { GradeComponent } from './Modules/Grade/index';
import { GradesComponent } from './Modules/Grades/index';
// partials
import { AuthorizedHeader, AuthorizedFooter, Navbar } from './Partials/index';

const appRoutes: Routes = [
    {
        path: 'auth',
        component: AuthLayoutComponent,
        loadChildren: './Modules/Auth/auth.module#AuthModule'
    },
    {
        path: 'grades',
        component: MainLayoutComponent,
        canActivate: [AuthGuard],
        children: [
            { path: 'my', component: GradesComponent },
            { path: ':id', component: GradeComponent },

            { path: '' , component: AuthorizedHeader, outlet: 'header'},
            { path: '' , component: AuthorizedFooter, outlet: 'footer'},
            { path: '' , component: Navbar, outlet: 'navbar'}

        ]       
    },
    // {
    //     path: 'sujects',
    //     component: MainLayoutComponent,
    //     loadChildren: './Modules/Subjects/subjects.module#SalesModule'
    // },

    {
        path: 'subjects',
        component: MainLayoutComponent,
        canActivate: [AuthGuard],
        children: [
            { path: 'index', component: SubjectsComponent  },

            { path: '' , component: AuthorizedHeader, outlet: 'header'},
            { path: '' , component: AuthorizedFooter, outlet: 'footer'},
            { path: '' , component: Navbar, outlet: 'navbar'}

        ]
    },


    {
        path: 'dashboards',
        component: MainLayoutComponent,
        canActivate: [AuthGuard],
        children: [
            { path: 'index', component: HomeComponent  },

            { path: '' , component: AuthorizedHeader, outlet: 'header'},
            { path: '' , component: AuthorizedFooter, outlet: 'footer'},
            { path: '' , component: Navbar, outlet: 'navbar'}

        ]
    },


    // domyślna podstrona
    { path: '**', redirectTo: 'dashboards/index' },


];

//export const routing = RouterModule.forRoot(appRoutes, { useHash: false });


@NgModule({
  imports: [ RouterModule.forRoot(appRoutes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}

