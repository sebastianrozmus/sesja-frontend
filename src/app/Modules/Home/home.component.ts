import { Component, OnInit } from '@angular/core';
// import { User } from '../_models/index';
// import { UserService } from '../_services/index';

import { Http, Headers, Response } from '@angular/http';

@Component({    
    templateUrl: 'home.component.html',
    styleUrls: [ './home.component.scss' ]
})

export class HomeComponent {

	public isLoading: boolean = true;
	public exams: any;
    public role: string;
	
    constructor( private http: Http ) { 

        this.role = JSON.parse(localStorage.getItem('user')).roles[0]

        this.http.get('http://localhost:8000/app_dev.php/api/exams/'+JSON.parse(localStorage.getItem('user')).token )
            .subscribe(
                data => {
                	console.log(data.json());
                	this.isLoading = false;
                	this.exams = data.json().data;
                },
                error => {

                	alert('error')
            });


    }
}