import { Component, OnInit } from '@angular/core';
// import { User } from '../_models/index';
// import { UserService } from '../_services/index';

import { Http, Headers, Response } from '@angular/http';

@Component({    
    templateUrl: 'subjects.component.html',
    styleUrls: [ './subjects.component.scss' ]
})

export class SubjectsComponent {

	public isLoading: boolean = true;
	public exams: any;
	
    constructor( private http: Http ) { 
        this.http.get('http://localhost:8000/app_dev.php/api/exams/'+JSON.parse(localStorage.getItem('user')).token )
            .subscribe(
                data => {
                	console.log(data.json());
                	this.isLoading = false;
                	this.exams = data.json().data;
                },
                error => {

                	alert('error')
            });


    }
}