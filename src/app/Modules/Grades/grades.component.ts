import { Component, OnInit } from '@angular/core';
// import { User } from '../_models/index';
// import { UserService } from '../_services/index';

import { Http, Headers, Response } from '@angular/http';
import { Router, ActivatedRoute } from '@angular/router';


import { FormBuilder, Validators, FormGroup, FormControl } from '@angular/forms';

@Component({    
    templateUrl: 'grades.component.html',
    styleUrls: [ './grades.component.scss' ]
})

export class GradesComponent {

	public isLoading: boolean = true;
	public grades: any;
    public avg: any;


	
    constructor( private http: Http, private route: ActivatedRoute, private formbuilder: FormBuilder) { 

            this.http.get('http://localhost:8000/app_dev.php/api/grades/my/'+JSON.parse(localStorage.getItem('user')).token )
                .subscribe(
                    data => {
                    	console.log(data.json());
                    	this.isLoading = false;
                    	this.grades = data.json().data;

                        let gradeSum: number = 0;
                        let cnt = 0;
                        for (var i = this.grades.length - 1; i >= 0; i--) {
                            if( this.grades[i].grade > 0 ) {
                                cnt++;
                                gradeSum += parseFloat( this.grades[i].grade )
                            }
                        }

                        this.avg = (gradeSum / cnt).toFixed(2)
                    },
                    error => {

                    	alert('error')
                });

    } 
 
}