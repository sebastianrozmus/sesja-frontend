import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './Login/login.component';
import { ReactiveFormsModule } from '@angular/forms';
import { AuthHeader, AuthFooter } from '../../Partials/index';

const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent
  },

  // partiale
  { 
    path: '', 
    component: AuthHeader, 
    outlet: 'header'
  },
  { 
    path: '', 
    component: AuthFooter, 
    outlet: 'footer'
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule, ReactiveFormsModule]
})
export class AuthRoutingModule {} 