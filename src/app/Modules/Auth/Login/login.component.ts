import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { AlertService, AuthenticationService, LoaderService } from '../../../Services/index';
import { FormBuilder, Validators, FormGroup, FormControl } from '@angular/forms';

@Component({
    templateUrl: 'login.component.html',
    providers: [FormBuilder],
    styleUrls: [ '../auth.component.scss' ]
})

export class LoginComponent implements OnInit {
    model: any = {};
    returnUrl: string;

    loginForm: FormGroup;
    username: FormControl = new FormControl("",Validators.required);
    password: FormControl = new FormControl("",Validators.required);

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private authenticationService: AuthenticationService,
        private alertService: AlertService,
        private loaderService: LoaderService,
        formbuilder: FormBuilder) { 

          // create form
          this.loginForm = formbuilder.group({
              'username': this.username,
              'password': this.password
          });

    }

    ngOnInit() {

        // wlogowujemy
        this.authenticationService.logout();

        // po zalogowaniu przekierujemy na referera
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';

    }

    login(username: string, password: string) {
        this.loaderService.start();
        this.authenticationService.login(username, password)
            .subscribe(
                data => {
                console.log(data);
                console.log(data.json());
                    localStorage.setItem('user', JSON.stringify(data.json()))
                    this.loaderService.end();
                    this.router.navigate(["/"]);
                },
                error => {
                    this.alertService.error("Niepoprawne dane logowania");
                    this.loaderService.end();
            });
    }

    onSubmit(e) {
        e.preventDefault();
        this.login(this.username.value, this.password.value);

        return false;
    }
 
}
