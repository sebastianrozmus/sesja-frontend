import { NgModule } from '@angular/core';
import { LoginComponent } from './Login/login.component';
import { AuthRoutingModule } from './auth-routing.module';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../Shared/shared.module';
import { AuthHeader, AuthFooter } from '../../Partials/index';

@NgModule({
  imports: [
    AuthRoutingModule,
    ReactiveFormsModule,
    SharedModule,
    
  ],
  declarations: [ 
    LoginComponent, 

    AuthHeader, 
    AuthFooter
  ],
  providers: [ 
  ]
})
export class AuthModule { }
