import { Component, OnInit } from '@angular/core';
// import { User } from '../_models/index';
// import { UserService } from '../_services/index';

import { Http, Headers, Response } from '@angular/http';
import { Router, ActivatedRoute } from '@angular/router';


import { FormBuilder, Validators, FormGroup, FormControl } from '@angular/forms';

@Component({    
    templateUrl: 'grade.component.html',
    styleUrls: [ './grade.component.scss' ]
})

export class GradeComponent {

	public isLoading: boolean = true;
	public exams: any;

    gradeForm: FormGroup;
    grades: FormControl = new FormControl("",Validators.required);
	
    constructor( private http: Http, private route: ActivatedRoute, private formbuilder: FormBuilder) { 

        this.gradeForm = formbuilder.group({
              'grades': this.grades,
        });

        this.route.params.subscribe(params => {
           
            this.http.get('http://localhost:8000/app_dev.php/api/grades/'+params['id'] )
                .subscribe(
                    data => {
                    	console.log(data.json());
                    	this.isLoading = false;
                    	this.exams = data.json().data;
                    },
                    error => {

                    	alert('error')
                });

       });

    }

    onSubmit(e) {
        e.preventDefault();
        this.route.params.subscribe(params => {
            let vals = {};
            for (var i = document.getElementsByTagName('input').length - 1; i >= 0; i--) {
                vals[document.getElementsByTagName('input')[i].name] = document.getElementsByTagName('input')[i].value
            }            

            this.http.post('http://localhost:8000/app_dev.php/api/grades/'+params['id'], { 'grades': vals } )
                .subscribe(
                    data => {
                        console.log(data.json());
                        this.isLoading = false;
                        alert('Zapisano oceny')
                    },
                    error => {

                        alert('error')
                });

       });

        return false;
    }
 
}