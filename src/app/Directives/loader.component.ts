import { Component, OnInit } from '@angular/core';

import { LoaderService } from '../Services/index';

@Component({
    selector: 'loader',
    templateUrl: './loader.component.html'
})

export class LoaderComponent {
    message: any;

    constructor(private loaderService: LoaderService) { }

    ngOnInit() {
        this.loaderService.getMessage().subscribe(message => { this.message = message; });
    }
}