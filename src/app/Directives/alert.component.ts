import { Component, OnInit } from '@angular/core';

import { AlertService } from '../Services/index';

@Component({
    selector: 'alerts',
    templateUrl: 'alert.component.html'
})

export class AlertComponent {
    message: any;

    constructor(private alertService: AlertService) { }

    ngOnInit() {
        this.alertService.getMessage().subscribe(message => { this.message = message; });
    }
}