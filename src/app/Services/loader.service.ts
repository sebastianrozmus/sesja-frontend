import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Subject } from 'rxjs/Subject';


@Injectable()
export class LoaderService {
    private subject = new Subject<any>();

    loaders: string[];

    constructor() {
    }

    start() {
        this.subject.next({})
    }

    end() {
        this.subject.next()
        this.subject.next()
        this.subject.next()
    }


    getMessage(): Observable<any> {
        return this.subject.asObservable();
    }
}