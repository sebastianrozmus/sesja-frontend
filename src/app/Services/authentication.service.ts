import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map'
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

@Injectable()
export class AuthenticationService {
    constructor(private http: Http, private router: Router) {
        if(localStorage.getItem('user')) {
            router.navigate(['/']);
        }
    }

    login(username: string, password: string) {
        return this.http.post('http://localhost:8000/app_dev.php/api/user/sign-in', JSON.stringify({ email: username, password: password }));

    }

    logout() {
        // remove user from local storage to log user out
        localStorage.removeItem('user');
        this.router.navigate(['/']);

    }
}