export * from './Auth/header.component';
export * from './Auth/footer.component';

export * from './Authorized/header.component';
export * from './Authorized/footer.component';
export * from './Authorized/navbar.component';