import { Component, OnInit } from '@angular/core';

@Component({
    templateUrl: './header.component.html',
    styleUrls: [ './header.component.scss' ]
})
export class AuthorizedHeader {
    public  show: boolean = false;
    public name: string = '';

    toggle() {
      this.show = !this.show
    }

    constructor() {

      this.name = JSON.parse(localStorage.getItem('user')).fullname

    }
 
}