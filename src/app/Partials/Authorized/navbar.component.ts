import { Component, OnInit } from '@angular/core';

@Component({
    templateUrl: './navbar.component.html',
    styleUrls: [ './navbar.component.scss' ]
})
export class Navbar {

	public role: string;

    constructor() {

    	this.role = JSON.parse(localStorage.getItem('user')).roles[0]

    }

}