import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app.routing';

import { SharedModule } from './Shared/shared.module';
import { MainLayoutComponent, AuthLayoutComponent } from './Layouts/index';

import { HomeComponent } from './Modules/Home/index';
import { SubjectsComponent } from './Modules/Subjects/index';
import { GradeComponent } from './Modules/Grade/index';
import { GradesComponent } from './Modules/Grades/index';
import { AuthenticationService, LoaderService } from './Services/index';
import { LoaderComponent } from './Directives/index';

import { AuthGuard } from './Guard/index';
import { Http, RequestOptions } from '@angular/http';

@NgModule({
  declarations: [
    AppComponent,
    MainLayoutComponent,
    AuthLayoutComponent,
    LoaderComponent,
    HomeComponent, 
    SubjectsComponent,
    GradeComponent,
    GradesComponent,
  ],
  imports: [
    BrowserModule,
    SharedModule,
    HttpModule,
    AppRoutingModule
  ],
  providers: [
    AuthGuard,
    
    LoaderService,
    AuthenticationService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
