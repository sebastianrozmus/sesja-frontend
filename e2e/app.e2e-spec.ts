import { SesjaFrontendPage } from './app.po';

describe('sesja-frontend App', () => {
  let page: SesjaFrontendPage;

  beforeEach(() => {
    page = new SesjaFrontendPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
